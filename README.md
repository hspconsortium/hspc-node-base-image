# README #

Base Docker image for HSPC Node projects.

Contains:

    - Node
    - npm
    - python
    - aws tools
    - aws ecs-cli
